public class Calculadora {
    public static void main(String[] args) {
        int a = 5;
        int b = 3;
        int suma = a + b;
        int resta = a - b;
        int multiplicacio = a * b;
        int divisio = 0;
        try {
            divisio = a / b;
        } catch (ArithmeticException e) {
            System.out.println("Error: Detectada divisió per zero");
        }
        System.out.println("La suma de " + a + " i " + b + " és: " + suma);
        System.out.println("La resta de " + a + " i " + b + " és: " + resta);
        System.out.println("La multiplicació de " + a + " i " + b + " és: " + multiplicacio);
        if (b != 0) {
            System.out.println("La divisió de " + a + " i " + b + " és: " + divisio);
        }
    }
}

